<?php

# Earlier

$name = isset($_GET['name']) ? $_GET['name'] : 'Default';

echo $name;

# PHP 7

$name = 'Sumit';

echo $name ?? 'Default';