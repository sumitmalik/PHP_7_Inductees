<?php

require 'vendor/autoload.php';

/*
Earlier imports were done individually

use App\Person;
use App\Animal;
*/

# PHP 7 grouped imports

use App\{Person, Animal};

var_dump(new Person);
var_dump(new Animal);