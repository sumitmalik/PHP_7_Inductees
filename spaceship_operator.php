<?php

# Example 1

$games = ['Counter Strike', 'Mass Effect', 'Fifa 17', 'Super Mario Bros', 'Call Of Duty', 'Battlefield'];

usort($games, function($a, $b) {
    return strlen($b) <=> strlen($a);
});

var_dump($games);

# Example 2

class User
{
    protected $name;
    protected $age;

    function __construct($name, $age)
    {
        $this->name = $name;
        $this->age  = $age;
    }

    public function name()
    {
        return $this->name;
    }

    public function age()
    {
        return $this->age;
    }
}

class UserCollection
{
    protected $users;

    public function __construct($users)
    {
        $this->users = $users;
    }

    public function users()
    {
        return $this->users;
    }

    public function sortBy($property)
    {
        usort($this->users, function($first_user, $second_user) use ($property) {
            return $first_user->$property() <=> $second_user->$property();
        });
    }
}

$collection = new UserCollection([
    new User('Sumit', 25),
    new User('Sahil', 23),
    new User('Amit', 27),
    new User('Tushar', 26)
]);

$collection->sortBy('age');

var_dump($collection->users());