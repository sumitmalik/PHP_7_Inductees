<?php

# By default, typehinting is not entirely strict.
# Following will enable strict type check.
# 
# declare(strict_types=1);

function accepted(int $age)
{
    var_dump($age);
}

accepted(30);
accepted('30');

function isValid(bool $var)
{
    echo '<br/>';
    var_dump($var);
}

isValid(true);
isValid('true');
isValid('sumit');
isValid(11);
isValid(1);
isValid(0);
isValid(-1);