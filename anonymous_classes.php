<?php

interface Logger
{
    public function log($message);
}

class Application
{
    protected $logger;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function action()
    {
        $this->logger->log('Doing Something');
    }
}

$app = new Application;

# use of anonymous class
$app->setLogger(new class implements Logger {
    public function log($message) {
        var_dump($message);
    }
});

$app->action();